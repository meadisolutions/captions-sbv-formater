package com.kobesolutions.captionformater.model;

public class CaptionLine {

    private String timeLineFrom;
    private String timeLineTo;
    private String captionLine;

    public CaptionLine(String timeLineFrom, String timeLineTo, String captionLine) {
        this.timeLineFrom = timeLineFrom;
        this.timeLineTo = timeLineTo;
        this.captionLine = captionLine;
    }

    @Override
    public String toString() {
        return timeLineFrom + "," + timeLineTo + "\n" + captionLine ;
    }
}
