package com.kobesolutions.captionformater;

import com.kobesolutions.captionformater.model.CaptionLine;
import com.kobesolutions.captionformater.service.CaptionLineGenerator;
import com.kobesolutions.captionformater.service.PunctuationBased;
import com.kobesolutions.captionformater.service.WordCountBased;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.time.LocalTime;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class SbvFormatter {

    /**
     * java -cp captions-sbv-formater-1.0-SNAPSHOT.jar com.kobesolutions.captionformater.SbvFormatter "../src/main/resources/article_hagereseb.txt" "00:00:52.000" "00:00:58.000"
     * @param args
     */
    public static void main(String[] args) {
        try {
            String inputFilePath = args[0];  // "src/main/resources/testArticle.txt"
            String firstStartTimeLine = args[1];  // "00:00:52.000"
            String firstEndTimeLine = args[2];   // "00:00:58.000"

            String wordCountArg = "";
            if(args.length > 3){
                wordCountArg = args[3];
            }

//          CaptionLineGenerator captionLineGenerator = new WordCountBased(8);
            CaptionLineGenerator captionLineGenerator = new PunctuationBased(List.of("።","፡"));
            Path articlePath = Paths.get(inputFilePath);
            String articleAsString = Files.readString(articlePath);
            List<String> captionLines = captionLineGenerator.getCaptionLines(articleAsString);
            LocalTime fromTS = LocalTime.parse(firstStartTimeLine);
            LocalTime toTS = LocalTime.parse(firstEndTimeLine);
            List<CaptionLine> captionLineList = getCaptionLines(captionLines, fromTS, toTS);
            createSbvFile(inputFilePath, captionLineList);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private static void createSbvFile(String inputFilePath, List<CaptionLine> captionLineList) throws IOException {
        StringBuilder stringBuilder = new StringBuilder();
        for (CaptionLine captionLine : captionLineList) {
            stringBuilder.append(captionLine.toString());
            stringBuilder.append("\n");
            stringBuilder.append("\n");
        }
        String captionsFile = inputFilePath.replace(".txt", ".sbv");
        Path path = Paths.get(captionsFile);
        byte[] strToBytes = stringBuilder.toString().getBytes();
        Files.write(path, strToBytes);
        System.out.println("SBV file successfully created. " + captionsFile);
    }

    private static List<CaptionLine> getCaptionLines(List<String> captionLines, LocalTime fromTS, LocalTime toTS) {
        List<CaptionLine> captionLineList = new ArrayList<>();
        Scanner input = new Scanner(System.in);
        System.out.println("Enter / to begin.");
        String userInput = input.nextLine();
        if(userInput.equals("/")){
            System.out.println("Click \\ to end");
            String currentLine = captionLines.get(0);
            System.out.println(fromTS+","+toTS);
            System.out.println(currentLine);
            captionLineList.add(new CaptionLine(fromTS.toString(),toTS.toString(), currentLine));
            String nextInput = input.nextLine();
            int i = 1;
            while(!nextInput.equals("\\") && i<captionLines.size()){
                LocalTime then = LocalTime.now();
                currentLine = captionLines.get(i);
                System.out.println("\n"+currentLine);
                nextInput = input.nextLine();
                LocalTime now = LocalTime.now();
                long nanoDiff = now.toNanoOfDay() - then.toNanoOfDay();
                fromTS = toTS;
                toTS = fromTS.plusNanos(nanoDiff);
                CaptionLine captionLine = new CaptionLine(fromTS.toString(), toTS.toString(), currentLine);
                System.out.println("--------------------------------");
                System.out.println(captionLine);
                captionLineList.add(captionLine);
                i++;
            }
        }
        return captionLineList;
    }
}
