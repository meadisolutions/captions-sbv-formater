package com.kobesolutions.captionformater.service;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class PunctuationBased implements CaptionLineGenerator{

    private List<String> delimiters;

    public PunctuationBased(List<String> delimiters) {
        this.delimiters = delimiters;
    }

    @Override
    public List<String> getCaptionLines(String articleAsString) throws IOException {
        List<String> captionLines = new ArrayList<>();
        for(String sentence: articleAsString.split(delimiters.get(0))){
            if(!sentence.equals("") && !sentence.isEmpty()) {
                String[] phrases = sentence.split(delimiters.get(1));
                int length = phrases.length;
                for (int i = 0; i < length - 1; i++) {
                    String trimPhrase = phrases[i].trim();
                    captionLines.add(trimPhrase+delimiters.get(1));
                }
                captionLines.add(phrases[length - 1].trim()+delimiters.get(0));
            }
        }
        return captionLines;
    }
}
