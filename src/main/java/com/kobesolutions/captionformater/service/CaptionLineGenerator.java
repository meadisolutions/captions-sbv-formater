package com.kobesolutions.captionformater.service;

import java.io.IOException;
import java.util.List;

public interface CaptionLineGenerator {
    List<String> getCaptionLines(String articleAsString) throws IOException;
}