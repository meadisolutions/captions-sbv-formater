package com.kobesolutions.captionformater.service;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class WordCountBased implements CaptionLineGenerator {

    private int numOfWords;

    public WordCountBased(int numOfWords) {
        this.numOfWords = numOfWords;
    }

    @Override
    public List<String> getCaptionLines(String articleAsString) {
        List<String> captionLines = new ArrayList<>();
        List<String> words = Arrays.asList(articleAsString.split("\\s+"));
        int wordIndex = 0;
        StringBuffer sb = new StringBuffer();
        for (String word : words) {
            if(wordIndex < numOfWords){
                sb.append(word + " ");
                wordIndex++;
            }else {
                captionLines.add(sb.toString());
                wordIndex = 0;
                sb = new StringBuffer();
            }
        }
        return captionLines;
    }
}
